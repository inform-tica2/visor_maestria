var map = new ol.Map({
    target: 'mapa',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      })
    ],
    view: new ol.View({
      center: ol.proj.fromLonLat([-74.07, 4.6788]),
      zoom: 14
    })
  });


  function mostrarMarcador(){

    let cuadroLatitud= document.getElementById("latitud");
    let cuadroLongitud= document.getElementById("longitud");

    let latitud=cuadroLatitud.value;
    let longitud=cuadroLongitud.value;

   
    let reproyectadas= ol.proj.fromLonLat([longitud,latitud]);

    let punto= new ol.geom.Point(reproyectadas);

    let marcador= new ol.Feature({
        geometry:punto
    });

    let sourcevector=new ol.source.Vector({
        features:[marcador]
    });


    //El error era acá. 
    //Estaba pasando el objeto source vector pero
    //toca pasar es un JSON con un atributo source cuyo
    //valor sea source vector.
    let capaVector= new ol.layer.Vector({
      source:sourcevector
    });


    let configIcono={
        scale:1,
        src: "https://openlayers.org/en/latest/examples/data/icon.png",
        opacity:1
    }

    let icono=new ol.style.Icon(configIcono);

    let iconoEstilo=new ol.style.Style({image:icono});

    marcador.setStyle(iconoEstilo);

    map.addLayer(capaVector);

  }